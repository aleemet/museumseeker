// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"
import Vue from "vue";
import axios from "axios";
//import "./socket"; // To be used later

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

new Vue({
  el: "#museumsearch",
  data: {
    status: 1,
    friendliness: "",
    overall: "",
    feedStatus: 0,
    text: "",
    museum: {},
    score: 0,
    visits: [],
    museums: []
  },
  methods: {
    getMuseums: function () {
      axios
        .get("/api/museums")
        .then(response => {
          console.log(response)
          this.museums = response["data"]["museums"];

        })
      //.catch(error => console.log(error));
    },
    getMuseum: function (museumId) {
      axios
        .get("/api/museums/" + museumId)
        .then(response => {
          console.log(response);
          this.museum = response["data"]["museum"];
          this.status = 2;
          this.getMuseumVisits(museumId);
        })
      //.catch(error => console.log(error));
    },
    displayForm: function () {
      this.feedStatus = 1;
    },
    saveFeedback: function () {
      this.feedStatus = 0;
      if (this.friendliness === "" || this.overall === "" || this.text === "") {
        return
      }
      axios.post("/api/visits/new", {
        visit: {
          museumId: this.museum.id,
          userId: 5,
          score: Math.round((parseInt(this.overall) + parseInt(this.friendliness)) / 2),
          text: this.text
        }
      })
        .then(response => {
          console.log(response)
          this.visits = this.getMuseumVisits(this.museum.id);
        })
    },
    getMuseumVisits: function (museumId) {
      axios
        .get("/api/visits/" + museumId)
        .then(response => {
          console.log(response)
          this.visits = response["data"]["visits"];
          this.score = this.getAverageScore();
          //console.log(this.score);
        })
      //.catch(error => console.log(error));
    },
    getAverageScore: function () {
      let sum = 0;
      this.visits.forEach(function (element) {
        sum += element["score"];
      })
      let a = sum / this.visits.length;

      return a.toFixed(2);
    },
    goBackFromDetails() {
      this.status = 1;
    }

  },
  mounted: function () {
    //this.getMuseumVisits(3);
    //console.log(this.status);
    //this.getMuseum("1");
    //this.getLatestVisits(4);
    this.getMuseums();
    //console.log(this.status);
  }
})

new Vue({
  el: "#showcase",
  data: {
    latest_visits: []
  },
  methods: {
    getLatestVisits: function (n) {
      axios
        .get("/api/visits/latest/" + n)
        .then(response => {
          console.log(response)
          this.latest_visits = response["data"]["visits"];
          //console.log(this.score);
        })
      //.catch(error => console.log(error));
    },
  },
  mounted: function () {
    this.getLatestVisits(5);
  }
})