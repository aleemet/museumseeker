defmodule Museumseeker.Museum.Visit do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :museumid, :score, :text, :userid, :inserted_at]}

  schema "visits" do
    field :museumid, :integer
    field :score, :integer
    field :text, :string
    field :userid, :integer

    timestamps()
  end

  @doc false
  def changeset(visit, attrs) do
    visit
    |> cast(attrs, [:museumid, :userid, :score, :text])
    |> validate_required([:museumid, :userid, :score, :text])
  end
end
