defmodule Museumseeker.Museum.Museum do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :address, :country, :name, :opentimes, :picture, :tickets, :shortdesc, :longdesc]}

  schema "museums" do
    field :address, :string
    field :country, :string
    field :name, :string
    field :opentimes, :string
    field :picture, :string
    field :tickets, :string
    field :shortdesc, :string
    field :longdesc, :string

    timestamps()
  end

  @doc false
  def changeset(museum, attrs) do
    museum
    |> cast(attrs, [:name, :country, :address, :opentimes, :tickets, :picture, :shortdesc, :longdesc])
    |> validate_required([:name, :country, :address, :opentimes, :tickets, :picture, :shortdesc, :longdesc])
  end
end
