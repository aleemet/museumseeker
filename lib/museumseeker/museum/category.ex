defmodule Museumseeker.Museum.Category do
  use Ecto.Schema
  import Ecto.Changeset


  schema "categories" do
    field :category, :string
    field :museumid, :string

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:museumid, :category])
    |> validate_required([:museumid, :category])
  end
end
