defmodule MuseumseekerWeb.Router do
  use MuseumseekerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MuseumseekerWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/museums", PageController, :museums
  end

  # Other scopes may use custom stacks.
  scope "/api", MuseumseekerWeb do
    pipe_through :api

    get "/museums/:id", Api.MuseumController, :getMuseum
    get "/museums", Api.MuseumController, :getMuseums
    get "/museums/filter", Api.MuseumController, :getFilteredMuseums
    get "/visits/:id", Api.VisitController, :getMuseumVisits
    post "/visits/new", Api.VisitController, :postMuseumVisit
    get "/visits/latest/:n", Api.VisitController, :getLatestVisits

  end
end
