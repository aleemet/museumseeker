defmodule MuseumseekerWeb.Api.VisitController do
    use MuseumseekerWeb, :controller
    import Ecto.Query
    alias Ecto.Changeset
    alias Museumseeker.{Museum.Visit, Repo, Accounts.User, Museum.Museum}
  
    def getMuseumVisits(conn, %{"id" => museumId}) do
        query = from t in Visit,
                join: c in User, 
                where: t.userid == c.id and t.museumid == ^museumId, 
                order_by: [desc: t.inserted_at],
                select: %{name: c.name, picture: c.picture, score: t.score, text: t.text, date: t.inserted_at}
    
        result = Repo.all(query)
        #result |> Enum.map (fn %{"date" => a, } -> %{"date" => Date.from_iso8601!(a)} end)
        result = for %{name: a, picture: b, score: c, text: d, date: e} <- result, do: %{name: a, picture: b, score: c, text: d, date: Date.to_iso8601 e}

        put_status(conn, 200) |> json(%{visits: result})
    end

    def postMuseumVisit(conn, params) do

        visit_details = params["visit"]

        visit_params = %{
            museumid: visit_details["museumId"],
            userid: visit_details["userId"],
            score: visit_details["score"],
            text: visit_details["text"]
        }
    
        visit_changeset = Visit.changeset(%Visit{}, visit_params)
   
        insertresp = Repo.insert!(visit_changeset)

        put_status(conn, 201) |> json(%{msg: "Visit created"})
    end

    def getLatestVisits(conn, %{"n" => n}) do
        
        query = from t in Visit,
            join: c in User, 
            on: t.userid == c.id,
            join: m in Museum,
            on: t.museumid == m.id,
            order_by: [desc: t.inserted_at],
            limit: ^n,
            select: %{name: c.name, picture: c.picture, score: t.score, text: t.text, date: t.inserted_at, mname: m.name}

        result = Repo.all(query)
            #result |> Enum.map (fn %{"date" => a, } -> %{"date" => Date.from_iso8601!(a)} end)
        result = for %{name: a, picture: b, score: c, text: d, date: e, mname: f} <- result, do: %{name: a, picture: b, score: c, text: d, date: Date.to_iso8601(e), mname: f}
    
        put_status(conn, 200) |> json(%{visits: result})

    end

  end
  