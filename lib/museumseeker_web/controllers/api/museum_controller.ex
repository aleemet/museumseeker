defmodule MuseumseekerWeb.Api.MuseumController do
    use MuseumseekerWeb, :controller
    import Ecto.Query
    alias Ecto.Changeset
    alias Museumseeker.{Museum.Museum, Repo}
  
    def getMuseum(conn, %{"id" => museumId}) do
        query = from t in Museum, 
                where: t.id == ^museumId, 
                select: t
    
        result = Repo.one(query)
    
        put_status(conn, 200) |> json(%{museum: result})
    end

    def getMuseums(conn, _params) do
        query = from t in Museum, 
                select: t
    
        result = Repo.all(query)
    
        put_status(conn, 200) |> json(%{museums: result})
    end
    
    def getFilteredMuseums(conn, params) do

        filters = Changeset.cast(%Museum{}, params, [:name, :country, :address, :opentimes, :tickets]) |> Map.fetch!(:changes) |> Map.to_list
        
        result = Museum |> where(^filters) |> Repo.all
  
        put_status(conn, 200) |> json(%{museums: result})
      end 

  end
  