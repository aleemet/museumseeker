defmodule MuseumseekerWeb.PageController do
  use MuseumseekerWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
  def museums(conn, _params) do
    render conn, "museums.html"
  end
end
