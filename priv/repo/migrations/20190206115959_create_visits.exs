defmodule Museumseeker.Repo.Migrations.CreateVisits do
  use Ecto.Migration

  def change do
    create table(:visits) do
      add :museumid, :integer
      add :userid, :integer
      add :score, :integer
      add :text, :string

      timestamps()
    end

  end
end
