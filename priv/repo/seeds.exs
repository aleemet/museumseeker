# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Museumseeker.Repo.insert!(%Museumseeker.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Museumseeker.{Repo, Museum.Museum, Museum.Category, Museum.Visit, Accounts.User}

Repo.insert!(%Museum{address: "Ülikooli 18, Tartu", country: "Estonia", name: "University of Tartu Art Museum", opentimes: "Mon-Fri 11:00-17:00", picture: "images/UniversityofTartuArtMuseum.jpg", tickets: "Adults 6 € | Concessions 4 € | Family ticket 12 €", shortdesc: "Located in the historical University Main Building, which has become a symbolic landmark of the city of Tartu. The museum has the only antique art exhibition in Estonia.", longdesc: "The Museum’s permanent exhibition consists of sculptures – original size plaster cast copies of the greatest works from Archaic, Classical and Hellenistic Greece. They were acquired in the 1860s from museums in England, Germany, France and Italy in order to promote the students’ interest in and knowledge of art, history and culture. The Art Museum exhibits numerous original ancient objects: an Egyptian mummy, coins, vases, clay lamps and cuneiform tablets."})
Repo.insert!(%Museum{address: "Raekoja plats 18, Tartu", country: "Estonia", name: "Tartu Art Museum", opentimes: "Wed, Fri-Sun 11:00-18:00, Thu 11:00-20:00", picture: "images/TartuArtMuseum.JPG", tickets: "1 €", shortdesc: "With a rich past of over 75 years the museum has remained true to its founding principle of collecting and presenting new Estonian art.", longdesc: "Starting from the dream of Pallas art society the first exhibition was organised in 1940. The initial collection had 133 works. The situation became confusing during the occupation of the Soviet Union. During three quarters of a century, the museum has managed to be a meticulous, active and equal partner with artists, other institutions and its audience, despite serious economic and spatial shortcomings. The museum has remained true to its founding principle of collecting and presenting new Estonian art."})
Repo.insert!(%Museum{address: "Jaani 14, Tartu", country: "Estonia", name: "Nõunõu Studio-Gallery", opentimes: "Wed-Fri 10:00-15:00", picture: "images/NounouStudioGallery.jpg", tickets: "Free entrance", shortdesc: "Nõunõu is a ceramics studio gallery. It's a space for showcasing the latest works by local Tartu ceramic artists united by our passion and respect for this ancient artform.", longdesc: "Nõunõu is a ceramics studio gallery. It's a space for showcasing the latest works by local Tartu ceramic artists united by our passion and respect for this ancient artform. Clay – a generous gift of the earth – is our means of exploring the inner realms and communicating with the world. The varied styles of the Nõunõu artists range from Nordic simplicity and functionality to Japanese aesthetics. Here you can find ceramic objects from contemporary art to unique tableware. The most important aspect of our creative approach is the care and dedication we put into every stage of the process. This can be sensed when handling the pieces, allowing one to create a special and enjoyable relationship with each object. Being both a studio and a gallery, the atmosphere at Nõunõu is always bursting with creative energy. It is our greatest joy to share this energy, beauty and inspiration with you to create something that can communicate even without words."})
Repo.insert!(%Museum{address: "Vanemuise 26, Tartu", country: "Estonia", name: "Tartu Art House", opentimes: "Wed-Mon 12:00-18:00", picture: "images/TartuArtHouse.jpg", tickets: "Free entrance", shortdesc: "Tartu Artists’ Union has been organising exhibitions in the exhibition spaces of Tartu Art House since the building was completed in 1959. The exhibition plan is put together based on an open competition for one year.", longdesc: "Tartu Artists’ Union (MTÜ Tartu Kunstnike Liit) is a non-profit organisation comprising professional artists and art theorists. The aim of the union is to promote art, create opportunities for professional development, provide information about events and exhibitions held in Estonia as well as abroad. Other union tasks include creating opportunities for its members to take part in exhibitions, providing and gathering information, making professional contacts, promoting collaboration between organisations and artists locally as well as internationally. The spaces in Tartu Art House serve as a comfortable venue for holding personal exhibitions but also for organising bigger exhibition projects, which occupy all of the galleries. The exhibition plan is put together based on an open competition for one year. Approximately 30 exhibitions are held annually."})
Repo.insert!(%Museum{address: "Kastani 42, Tartu", country: "Estonia", name: "Kogo Gallery", opentimes: "Wed-Fri 12:00-19:00, Sat 12:00-18:00", picture: "images/KogoGallery.jpg", tickets: "Free entrance", shortdesc: "Kogo is a contemporary art gallery representing outstanding emerging and mid-career Estonian and international artists of all media.", longdesc: "Kogo is a contemporary art gallery representing outstanding emerging and mid-career Estonian and international artists of all media. Since its establishment in spring 2018, it has been Kogo’s aim to help the artists with their international visibility, to introduce their artistic practices as widely as possible and to create and maintain professional contacts with art aficionados and community of collectors. The gallery’s program is focused on artists’ solo projects, curatorial projects, and exhibitions born out of cooperation between artists. With our program, we support artistic freedom, interdisciplinarity, and freshness in ideas."})
Repo.insert!(%Museum{address: "Weizenbergi 34, Tallinn", country: "Estonia", name: "Kumu Art Museum", opentimes: "Tue-Wed, Fri-Sun 10:00-18:00, Thu 10:00-18:00", picture: "images/KumuArtMuseum.jpg", tickets: "Sponsor ticket 10 € | Adults 8 € | Concessions 6 € | Family ticket 16 €", shortdesc: "Multifunctional contemporary art museum and a place for conserving and exhibiting the world’s largest collection of Estonian art.", longdesc: "Kumu’s history is currently, to a great extent, a construction story. Today, the local, as well as international, public have become accustomed to the museum. An in-house rhythm of activities has developed. Slowly, we are starting to forget what a great shift Kumu caused in the reality of the Estonian art landscape. The Kumu story is being created, being born of Kumu’s exhibitions and activities, and its role as a carrier and transmitter of cultural values."})


Repo.insert!(%Category{category: "Art", museumid: "1"})
Repo.insert!(%Category{category: "Art", museumid: "2"})
Repo.insert!(%Category{category: "Art", museumid: "3"})
Repo.insert!(%Category{category: "Art", museumid: "4"})
Repo.insert!(%Category{category: "Art", museumid: "5"})
Repo.insert!(%Category{category: "Art", museumid: "6"})
Repo.insert!(%Category{category: "Art", museumid: "7"})
Repo.insert!(%Category{category: "Ceramics", museumid: "3"})

Repo.insert!(%User{name: "Art Elover", password: "password", username: "username", picture: "images/ArteLover.jpg"})
Repo.insert!(%User{name: "Muse Umgoer", password: "password", username: "username1", picture: "images/MuseUmgoer.jpg"})
Repo.insert!(%User{name: "Lady Art", password: "password", username: "username2", picture: "images/LadyArt.jpg"})
Repo.insert!(%User{name: "Art Seer", password: "password", username: "username3", picture: "images/ArtSeer.jpg"})
Repo.insert!(%User{name: "Iast Enthus", password: "password", username: "username4", picture: "images/IastEnthus.jpg"})
Repo.insert!(%User{name: "E. Kshib Ition", password: "password", username: "username5", picture: "images/EKshibItion.jpg"})

Repo.insert!(%Visit{museumid: 1, score: 10, text: "Perfect! I felt relief being in there, never wanted to leave", userid: 2})
Repo.insert!(%Visit{museumid: 1, score: 9, text: "Loveable", userid: 1})
Repo.insert!(%Visit{museumid: 1, score: 10, text: "The interior, the rarities in the exhibitions. LOVE", userid: 4})
Repo.insert!(%Visit{museumid: 1, score: 9, text: "I'm still in love of my university", userid: 3})

Repo.insert!(%Visit{museumid: 2, score: 7, text: "Nice", userid: 2})
Repo.insert!(%Visit{museumid: 2, score: 8, text: "Went with a friend, now we're smth more. Very intimate, thank u", userid: 3})
Repo.insert!(%Visit{museumid: 2, score: 9, text: "I enjoyed the exhibition", userid: 4})
Repo.insert!(%Visit{museumid: 2, score: 8, text: "Loveable", userid: 1})

Repo.insert!(%Visit{museumid: 3, score: 7, text: "Very good art", userid: 2})
Repo.insert!(%Visit{museumid: 3, score: 6, text: "Loveable", userid: 1})
Repo.insert!(%Visit{museumid: 3, score: 8, text: "Enjoyed the Nordic style :)", userid: 4})

Repo.insert!(%Visit{museumid: 4, score: 6, text: "Cute house ;)", userid: 2})
Repo.insert!(%Visit{museumid: 4, score: 6, text: "Enjoyed it a lot", userid: 4})
Repo.insert!(%Visit{museumid: 4, score: 7, text: "I wanted to show an exchange student Estonian art it was the perfect place", userid: 3})
Repo.insert!(%Visit{museumid: 4, score: 5, text: "Loveable", userid: 1})

Repo.insert!(%Visit{museumid: 6, score: 4, text: "A huuuge place, quite scary actually", userid: 2})
Repo.insert!(%Visit{museumid: 6, score: 6, text: "Loveable", userid: 1})
Repo.insert!(%Visit{museumid: 6, score: 8, text: "art, art, ART lots of it, very nice thanks", userid: 4})
Repo.insert!(%Visit{museumid: 6, score: 5, text: "so much to see, so much to do, did NOT manage in a day", userid: 3})
Repo.insert!(%Visit{museumid: 5, score: 7, text: "loved the lastest exhibition, hopefully the theme will continue", userid: 3})

Repo.insert!(%Visit{museumid: 5, score: 8, text: "A small place but always something new to see", userid: 2})
Repo.insert!(%Visit{museumid: 3, score: 2, text: "Maybe something for a ceramics lover, but that ain't me!", userid: 3})
Repo.insert!(%Visit{museumid: 5, score: 6, text: "Loveable", userid: 1})
Repo.insert!(%Visit{museumid: 6, score: 9, text: "there is so much to see and so much to do in KUMU, did NOT manage in 1 day", userid: 6})
Repo.insert!(%Visit{museumid: 5, score: 8, text: "KoGo might be my favourite place in TARTU", userid: 4})
