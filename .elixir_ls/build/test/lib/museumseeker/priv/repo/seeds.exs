# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Museumseeker.Repo.insert!(%Museumseeker.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Museumseeker.{Repo, Museum.Museum, Museum.Category, Museum.Visit, Accounts.User}

Repo.insert!(%Museum{address: "Ülikool 18, Tartu", country: "Estonia", name: "University of Tartu Art Museum", opentimes: "Mon-Fri 11:00-17:00", picture: "x", tickets: "Adults 6 € \n Concessions 4 € \n Family ticket 12 €"})
Repo.insert!(%Museum{address: "Raekoja plats 18, Tartu", country: "Estonia", name: "Tartu Art Museum", opentimes: "Wed, Fri-Sun 11:00-18:00 \n Thu 11:00-20:00", picture: "x", tickets: "1 €"})
Repo.insert!(%Museum{address: "Jaani 14, Tartu", country: "Estonia", name: "Nõunõu Studio-Gallery", opentimes: "Wed-Fri 10:00-15:00", picture: "x", tickets: "Free entrance"})
Repo.insert!(%Museum{address: "Gildi 2, Tartu", country: "Estonia", name: "Gildi Gallery", opentimes: "Mon-Fri 10:00-17:30 \n Sat 10:00-15:00", picture: "x", tickets: "Free entrance"})
Repo.insert!(%Museum{address: "Vanemuise 26, Tartu", country: "Estonia", name: "Tartu Art House", opentimes: "Wed-Mon 12:00-18:00", picture: "x", tickets: "Free entrance"})
Repo.insert!(%Museum{address: "Kastani 42, Tartu", country: "Estonia", name: "Kogo Gallery", opentimes: "Wed-Fri 12:00-19:00 \n Sat 12:00-18:00", picture: "x", tickets: "Free entrance"})
Repo.insert!(%Museum{address: "Weizenbergi 34, Tallinn", country: "Estonia", name: "Kumu Art Museum", opentimes: "Tue,Wed,Fri,Sat,Sun 10:00-18:00 \n Thu 10:00-18:00", picture: "x", tickets: "Sponsor ticket 10 € \n Adults 8 € \n Concessions 6 € \n Family ticket 16 €"})


Repo.insert!(%Category{category: "Art", museumid: "1"})
Repo.insert!(%Category{category: "Art", museumid: "2"})
Repo.insert!(%Category{category: "Art", museumid: "3"})
Repo.insert!(%Category{category: "Art", museumid: "4"})
Repo.insert!(%Category{category: "Art", museumid: "5"})
Repo.insert!(%Category{category: "Art", museumid: "6"})
Repo.insert!(%Category{category: "Art", museumid: "7"})
Repo.insert!(%Category{category: "Ceramics", museumid: "3"})

Repo.insert!(%User{name: "Art Elover", password: "password", username: "username"})

Repo.insert!(%Visit{museumid: "3", score: 4, text: "Loveable", userid: "1"})