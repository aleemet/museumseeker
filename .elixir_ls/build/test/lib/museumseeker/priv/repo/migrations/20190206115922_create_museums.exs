defmodule Museumseeker.Repo.Migrations.CreateMuseums do
  use Ecto.Migration

  def change do
    create table(:museums) do
      add :name, :string
      add :country, :string
      add :address, :string
      add :opentimes, :string
      add :tickets, :string
      add :picture, :string

      timestamps()
    end

  end
end
